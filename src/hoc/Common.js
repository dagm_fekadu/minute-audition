import React from 'react'
import { results } from '../reducers/reducers';


export const WithLoading = (Component)=>{
        return ({loading,...props}) => {
                if(!loading) return  (<Component {...props}></Component>)
        }
};
         

export const WithError = ({errors,children}) => (
        errors  ? <div className="error">{errors.messages}</div> :  children
)       

module.exports = {
        WithLoading,
        WithError
};