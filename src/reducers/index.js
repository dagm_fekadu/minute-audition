import { combineReducers } from 'redux'
import {loading, results,query,keywords,errors,socialMedia} from './reducers'

export default combineReducers({
 loading, results, query,keywords,errors,socialMedia
})
