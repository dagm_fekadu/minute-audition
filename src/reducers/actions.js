import { DELETE_KEYWORD, KEYWORD_ADDED, KEYWORDS_FETCH_COMPLETED, SEARCH_COMPLETED,SET_MEDIA } from './reducers'
import { fetchWithDispatch } from './fetch'
import config from '../config'

/**
 *
 * @param p parameter type object { url : url , p : search text}: currently showing the only popular tweets for twitter 
 * @param flag whether to use a callback function or not, since the callback function is optional
 * @returns {Function}
 */
export const searchCompleted = (p,flag) => {
  console.log("searchTwitterCompleted q",p);
  const query = p.query ? p.query : p;
  let url = null
  switch (p.media) {
    case "twitter":
      url =  config.TWITTER_SEARCH + p +"/popular"
      break
    case "facebook":
      url = config.TWITTER_SEARCH + p 
      break
    case "vk":
      url = config.VK_SEARCH + p
      break
    default:
      url = config.TWITTER_SEARCH + p
  }
  return fetchWithDispatch({
    type: SEARCH_COMPLETED,
    url : url,
    callbackFunction : flag ? function(d,r) {
      return keywordAdded(query)(d)
    } : null
  })
}


export const keywordsFetch = (flag) => {
  return fetchWithDispatch({
    type: KEYWORDS_FETCH_COMPLETED,
    url: config.KEYWORDS,
    callbackFunction : flag ? (d,r)=>{
      const text =r[0].text
      return searchCompleted(text)(d)
    }:null
  })
}

export const keywords = () => {
    return fetchWithDispatch({
      type: KEYWORDS_FETCH_COMPLETED,
      url: config.KEYWORDS
    })
}

export const keywordAdded = (p) => {
  return fetchWithDispatch({
    type: KEYWORD_ADDED,
    url : config.KEYWORDS,
    options : {text : p}
  })
}

export const deleteKeyword = id => {
  return fetchWithDispatch({
    type: DELETE_KEYWORD,
    url: config.KEYWORDS+id
  })
}

export const socialMedia = i =>{
  return {
    type :SET_MEDIA,
    payload : i
  }
}