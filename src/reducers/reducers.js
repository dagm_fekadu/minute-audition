export const LOADING = 'LOADING'
export const SEARCH_COMPLETED = 'SEARCH_COMPLETED'
export const KEYWORDS_FETCH_COMPLETED = 'KEYWORDS_FETCH_COMPLETED'
export const DELETE_KEYWORD = 'DELETE_KEYWORD'
export const KEYWORD_ADDED = 'KEYWORD_ADDED'
export const ERROR = 'ERROR'
export const SET_MEDIA = 'SET_MEDIA'

//TODO : Fix the backend to match redux state and reducer function

export const loading = (state = false, action) => {
  switch (action.type) {
    case LOADING:
      return !state
    default:
     return state
  }
}

export const query = (state = null, action) => {
  switch (action.type) {
    default:
      return state
  }
}

export const results = (state = null, action) => {
  switch (action.type) {
    case SEARCH_COMPLETED:
      return action.payload
    default:
      return state
  }
}

export const keywords = (state = null, action) => {
  switch (action.type) {
    case KEYWORDS_FETCH_COMPLETED:
      return action.payload
    case DELETE_KEYWORD:
      const index = state.map(e=>e.id).indexOf(action.payload.id)
      return [...state.slice(0,index),...state.slice(index+1,state.length)]
    case KEYWORD_ADDED:
      return [...state,action.payload]
    default:
      return state
  }
}

export const errors = (state = null, action) => {
  switch (action.type) {
    case ERROR:
      return action.payload ? action.payload : !state
    default:
      return state
  }
}

export const socialMedia=(state = 'twitter',action) => {
  switch(action.type){
    case SET_MEDIA:
      return action.payload;
    default:
      return state;
  }
}