import { LOADING } from './reducers'

const JSON_HEADER = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

 const request = (url,options)=>{
  options = options ? {method : "post",headers:JSON_HEADER, body : JSON.stringify(options)}:null
  return fetch(url,options).then(r=>{
    return r.json()
  })
}

//Todo: this callback function is optional, you can play with the result and do staff with dispatch as well.
/**
 *
 * @param url : the url to request
 * @param type : the main action type for the dispatch
 * @param callbackFunction : in case you want to manipulate the result through a
 * call back or pass it to another action, the dispatch and the
 * result are passed to you
 * @param options  : this is an options for fetching from the server, default is null
 * @returns {Function}  Returns function that accepts a dispatch and run async code and dispatch it.
 */
export const fetchWithDispatch = ({ url, type, callbackFunction, options }) => {
  return d => {
    d({ type: LOADING })
    request(url, options).then(r => {
      if (r.errors) {
        d({ type: LOADING })
        d({ type: true, payload: r.errors })
      } else {
        d({ type: LOADING })
        if (callbackFunction)
          callbackFunction(d, r)
        d({ type: type, payload: r })
      }
    }).catch(error => {
      d({ type: LOADING })
      d({
        type: 'ERROR',
        payload: { message: error.toString(), error: true }
      })
    })
  }
}