import React from "react";
import { Icon, Button } from "@blueprintjs/core";
import PropTypes from 'prop-types'

const ListItem = ({ id, text, icon, onItemClick, onIconClick }) => (
  <div className="simple-menu">
    <span
      onClick={e => {
        onItemClick(text);
      }}
    >
      {text}
    </span>
    <span>
      <Icon
        icon={icon}
        onClick={e => {
          onIconClick(id);
        }}
      />
    </span>
  </div>
);

class SnackBar extends React.Component {
  constructor(props) {
    super(props);
    const style = "show i-" + this.props.type 
    this.state = {
      style: style,
      delay : this.props.delay
    }
    this.hide = this.hide.bind(this);
  }

  componentWillMount() {
    var that = this;
    setTimeout(function() {
      that.hide();
    }, this.state.delay);
  }

  hide() {
    this.setState(({ state, props }) => {
      return {
        style: null
      };
    });
  }

  render() {
    return (
      <div id="snackbar" className={this.state.style}>
        <span>
          {this.props.message}
        </span>
        <button className="snack-close" onClick={this.hide}>
          Close
        </button>
      </div>
    );
  }
}

SnackBar.defaultProps = {
  type : "",
  delay : 5000,
  message : "Unexpected error occured!"
}

module.exports = { ListItem, SnackBar };
