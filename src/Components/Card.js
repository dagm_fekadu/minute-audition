import { Card, Elevation, Tag } from '@blueprintjs/core'
import React from 'react'

const CardItem = ({
                    img, name, created_at, favorite_count, retweet_count,
                    text, loading, reposts, likes, comments, id, url
                  }) => {
  return !loading ? <Card selevation={Elevation.THREE} className="v-aligned-spaced">
      <div className="card-header">
        <div>
          {img && <img className="profile-image" alt={'User profile'} src={img}/>}
        </div>
        <div className="card-title">
          {name && <span>{name}</span>}
          <small>{new Date(created_at).toDateString()}</small>
        </div>
      </div>
      <div className="card-text" dangerouslySetInnerHTML={{
        __html : text.replace(
          /((http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?)/g,
          '<a href="$1" target="_blank">$1</a>'
        )
      }}></div>

      <br/>
      <div className="card-footer">
        <Tag icon={'heart'} large={false} minimal={true}> {favorite_count ? favorite_count : likes}</Tag>
        <Tag icon={'eye-open'} large={false} minimal={true}>  {retweet_count ? retweet_count : reposts} </Tag>
        {/*{comments && <Tag icon={'eye-open'} large={false} minimal={true}>  {comments} </Tag>}*/}
        {url && <a href={url}>  {'Visit post'} </a>}
      </div>
    </Card> :
    <Card selevation={Elevation.THREE}>
      <p className="bp3-skeleton">Some</p>
      <p className="bp3-skeleton">text</p>
      <p className="bp3-skeleton"> text </p>
      <br/>
      <div className="card-footer">
        <p className="bp3-skeleton">Some place</p>
        <p className="bp3-skeleton">Some place</p>
        <p className="bp3-skeleton">Some place</p>
      </div>
    </Card>
}

export default CardItem