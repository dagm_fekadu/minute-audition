const serverUrl ="http://localhost:3000/"
export default {
  serverUrl:serverUrl ,
  TWITTER_SEARCH : serverUrl + "search/twitter/",
  VK_SEARCH : serverUrl + "search/vk/",
  KEYWORDS :  serverUrl + "keywords/",
  env : "development",
  media : [
    "Twitter","VK","Facebook" 
  ]
}