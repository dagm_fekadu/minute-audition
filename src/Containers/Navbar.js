import React from "react";
import { push } from "connected-react-router";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { searchCompleted, socialMedia } from "../reducers/actions";
import { Navbar, Button, Alignment } from "@blueprintjs/core";
import config from "../config";

const Nav = props => {
  let inputValue = null;
  return (
    <div>
      <Navbar className="fixed">
        <Navbar.Group align={Alignment.LEFT}>
          <Navbar.Heading>Bruting Bubbles</Navbar.Heading>
        </Navbar.Group>
        <Navbar.Group align={Alignment.RIGHT} className="nav-bar-align">
          <select
            onChange={e => {
              props.setSocialMedia(e.target.value.toLowerCase());
            }}
          >
            {config.media.map(i => {
              return (
                <option key={i} value={i} onClick={() => {}}>
                  {i}
                </option>
              );
            })}
          </select>
          <div className="bp3-input-group .modifier">
            <button className="bp3-button bp3-minimal bp3-intent-warning bp3-icon-search" />
            <input
              type="text"
              className="bp3-input"
              ref={e => {
                inputValue = e;
              }}
              placeholder="Enter keyword"
              id="query"
            />
          </div>
          <Navbar.Divider />
          <Button
            className="bp3-minimal"
            text="Search"
            onClick={e => {
              props.searchCompleted(inputValue.value, true);
            }}
          />
        </Navbar.Group>
      </Navbar>
      {props.loading ? <div className="loader" /> : null}
    </div>
  );
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      searchCompleted,
      setSocialMedia: socialMedia,
      changePage: () => push("/about-us")
    },
    dispatch
  );
const mapStateToProps = state => {
  return {
    loading: state.loading,
    socialMedia: state.socialMedia
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Nav);
