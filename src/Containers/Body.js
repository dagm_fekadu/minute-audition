import React from "react";
import { connect } from "react-redux";
import CardItem from "../Components/Card";
import {
  keywordsFetch,
  searchCompleted,
  deleteKeyword
} from "../reducers/actions";
import { Icon, MenuDivider } from "@blueprintjs/core";
import { bindActionCreators } from "redux";
import { ListItem, SnackBar } from "../Components/Commons";

class Body extends React.Component {
  componentDidMount() {
    this.props.keywordsFetch(true);
  }

  renderKeywords() {
    if (!this.props.keywords) return;
    return (
      <div className="bp3-card fill-height">
        {this.props.keywords.map(v => (
          <ListItem
            id={v.id}
            key={v.id}
            text={v.text}
            onItemClick={this.props.searchCompleted}
            onIconClick={this.props.deleteKeyword}
            icon={"remove"}
          />
        ))}
      </div>
    );
  }

  renderResults() {
    const Loading = [1, 2, 3].map(p => {
      <CardItem loading={true} key={p} />;
    });

    if (this.props.errors)
      return (
        <SnackBar message={this.props.errors.message} type="error"/>
      );
    if (!this.props.results) return Loading;
    if (!this.props.results.length)
      return <SnackBar message="Found no results for the current keyword!" />;
    return (
      <div className="grid-item">
        {this.props.results.map(p => {
          return <CardItem {...p} key={p.id} />;
        })}
      </div>
    );
  }

  render() {
    return (
      <div className="grid">
        {this.renderKeywords()}
        {this.renderResults()}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  results: state.results,
  keywords: state.keywords,
  loading: state.loading,
  errors: state.errors
});
const mapDispatchToProps = dispath =>
  bindActionCreators(
    {
      keywordsFetch: keywordsFetch,
      searchCompleted: searchCompleted,
      deleteKeyword: deleteKeyword
    },
    dispath
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Body);
