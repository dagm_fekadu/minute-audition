import React from 'react'
import { Route } from 'react-router-dom'
import Navbar from './Navbar'
import Body from './Body'

const App = () => (
  <main className="">
    <Route exact path="/" component={Navbar} className="fixed" />
    <Route exact path="/" component={Body} />
  </main>
)

export default App
